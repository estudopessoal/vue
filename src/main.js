import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './containers/App.vue'
import router from './router'
import store from './store'

Vue.use(VueResource)

new Vue({
	el: '#app',
	store,
	router,
	render: h => h(App)
})
