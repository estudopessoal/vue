import Vue from 'vue'
import VueRouter from 'vue-router'
import { routes } from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	routes
})

router.beforeEach(function (to, from, next) {
	window.scrollTo(0, 0)
	next()
})

export default router
export { routes }