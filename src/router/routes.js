import Home from '../containers/Home.vue'
import Sobre from '../containers/Sobre.vue'
export const routes = [
  {
    path: '/',
    component: Home,
    name: 'home',
    title: 'Home',
    all: true
  },
  {
    path: '/sobre',
    component: Sobre,
    name: 'sobre',
    title: 'Sobre',
    navbar: true,
    all: true
  }
]